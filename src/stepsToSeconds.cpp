#include <string>
#include <chrono>
#include <cmath>

/// beats = bpm * minutes
// steps / 4 = bpm * (seconds / 60)
// steps / 4 = (bpm * seconds) / 60
// seconds = (steps * 60) / (4 * bpm)
/// seconds = (steps * 15) / bpm
// milliseconds / 1000 = (steps * 15) / bpm
/// milliseconds = (steps * 15000) / bpm

std::string stepsToSeconds(const std::string &steps) {
    return std::to_string((stod(steps) * 15) / programOptions.bpm);
}

std::chrono::milliseconds stepsToMilliseconds(const std::string &steps) {
    return std::chrono::milliseconds(lround((stod(steps) * 15000) / programOptions.bpm));
}
