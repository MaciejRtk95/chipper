#ifndef CHIPPER_SEQUENCE_H
#define CHIPPER_SEQUENCE_H

#include <vector>
#include <iostream>
#include <fstream>
#include "getNextWord.h"
#include "isANote.h"
#include "playNote.h"
#include "pause.h"

class Sequence {
public:
    std::string name;
    std::vector<std::string> lines;

    void clear() {
        lines.clear();
    }

    void addLine(const std::string &line) {
        lines.push_back(line);
    }
};

#endif //CHIPPER_SEQUENCE_H
