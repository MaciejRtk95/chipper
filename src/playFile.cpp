#include <fstream>
#include <iostream>
#include "getNextWord.h"
#include "isANote.h"
#include "playNote.h"
#include "pause.h"
#include "SequenceVector.h"

void playFile(const std::string &fileName) {
    std::ifstream file;
    file.open(fileName);

    std::string currentLine;
    getline(file, currentLine);
    if (!programOptions.bpmSetFromProgramArguments) {
        programOptions.bpm = stod(currentLine);
        programOptions.bpm *= programOptions.multiplier;
    }

    SequenceVector sequences;
    std::string buffer;
    while (!file.eof()) {
        getline(file, currentLine);

        if (!currentLine.empty()) {
            int i = 0;
            buffer = getNextWord(currentLine, i);
            if (isANote(buffer)) {
                playNote(currentLine);
            } else if (buffer == "P") {
                pause(currentLine);
            } else if (!buffer.empty()) {//It might have been a line with only whitespace. That's why we're checking there's some word here.
                std::string sequenceName = buffer;

                buffer = getNextWord(currentLine, i);
                if (buffer == ":" || sequenceName[sequenceName.length() - 1] == ':') {
                    if (sequenceName[sequenceName.length() - 1] == ':') {
                        sequenceName.pop_back();
                    }
                    sequences.define(sequenceName, file);
                } else {
                    if (isAnInteger(buffer)) {
                        sequences.play(sequenceName, std::stoi(buffer));
                    } else {
                        sequences.play(sequenceName);
                    }
                }
            }
        }
    }
}
