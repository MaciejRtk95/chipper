#include <iostream>

void printError(const std::string &error, const std::string &fileName, const int lineCount, int returnCode) {
    std::cout << "ERROR in file \"" << fileName << "\", line " << lineCount << '\n'
              << error << std::endl;
    exit(returnCode);
}

void printError(const std::string &error, int returnCode) {
    std::cout << "ERROR: " << error << std::endl;
    exit(returnCode);
}

void printError(const std::string &error, const std::string &fileName, int returnCode) {
    std::cout << "ERROR in file \"" << fileName << "\"\n"
              << error << std::endl;
    exit(returnCode);
}

void printImpossibleError(const std::string &message) {
    std::cout << "\033[1;31mIMPOSSIBLE ERROR: the developers probably screwed up a bit too much\n"
              << message << "\033[0m" << std::endl;
    exit(95);
}
