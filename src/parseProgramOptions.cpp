#include <iostream>
#include <getopt.h>
#include "ProgramOptions.h"
#include "printUsage.h"
#include "isAValidBPM.h"
#include "printError.h"

void parseProgramOptions(int argc, char **argv) {
    programOptions.programCall = argv[0];

    if (argc == 1) {
        printUsage();
    }

    int longIndex = 0;
    int opt = getopt_long(argc, argv, shortOptions, longOptions, &longIndex);
    while (opt != -1) {
        switch (opt) {
            case 'v':
                programOptions.verbosity++;
                break;
            case '?':
            case 'h':
                printUsage();
                break;
            case 's':
                programOptions.simulationMode = true;
                break;
            case 'b':
                if (!isAValidBPM(optarg)) {
                    printError("BPM supplied in program arguments is invalid", 1);
                }
                programOptions.bpm = std::stod(optarg);
            programOptions.bpmSetFromProgramArguments = true;
                break;
            case 'm':
                if (!isAValidBPM(optarg)) {//Incidentally, the multiplier has the same constraints as the bpm
                    printError("BPM multiplier supplied in program arguments is invalid", 1);
                }
                programOptions.multiplier = std::stod(optarg);
                break;
            default:
                std::cout << "\033[1;31mTHE END OF THE WORLD IS HERE!!!\033[0m" << std::endl;
                std::cout << "Not really, actually, sorry, the developer just screwed up. Let him know of that (tell him it's something about the parameters) by making an issue here: https://www.gitlab.com/matsaki95/chipper" << std::endl;
                exit(1);
        }
        opt = getopt_long(argc, argv, shortOptions, longOptions, &longIndex);
    }

    if (optind >= argc) {
        std::cout << "No files to be played were specified. Run " << programOptions.programCall << " -h for help." << std::endl;
        exit(1);
    }
    for (int i = optind; i < argc; i++) {
        programOptions.filesToPlay.emplace_back(argv[i]);
    }

    if (programOptions.bpmSetFromProgramArguments) {
        programOptions.bpm *= programOptions.multiplier;
    }
}
