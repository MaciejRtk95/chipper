#include <string>
#include "getNextWord.h"
#include "isANumber.h"
#include "printError.h"

void checkNoteLineValidity(const std::string &line, const std::string &fileName, const int lineCount) {
    int i = 0;
    getNextWord(line, i);//Skipping the note part, assuming it's already checked

    //Duration
    std::string buffer = getNextWord(line, i);
    if (buffer.empty()) {
        printError("No duration specified", fileName, lineCount, 2);
    } else if (!isANumber(buffer)) {
        printError("\"" + buffer + "\" is not a valid duration", fileName, lineCount, 2);
    }

    //Optional extras
    buffer = getNextWord(line, i);
    if (!buffer.empty()) {
        if (isAnInteger(buffer)) {
            //Check if there's more!
            buffer = getNextWord(line, i);
            if (!buffer.empty()) {
                if (buffer != "&") {
                    printError("Invalid characters following repetition amount", fileName, lineCount, 2);
                } else {
                    //Confirming there's nothing after the ampersand
                    buffer = getNextWord(line, i);
                    if (!buffer.empty()) {
                        printError("Invalid characters following ampersand", fileName, lineCount, 2);
                    }
                }
            }
        } else if (buffer != "&") {
            printError("Invalid characters following duration", fileName, lineCount, 2);
        } else {
            //Confirming there's nothing after the ampersand
            buffer = getNextWord(line, i);
            if (!buffer.empty()) {
                printError("Invalid characters following ampersand", fileName, lineCount, 2);
            }
        }
    }
}
