#include <iostream>
#include <fstream>
#include <vector>
#include "isANumber.h"
#include "isANote.h"
#include "getNextWord.h"
#include "printError.h"
#include "isAValidBPM.h"
#include "checkNoteLineValidity.h"
#include "checkPauseLineValidity.h"
#include "fileValiditySequenceChecker.h"

void checkFileValidity(const std::string &fileName) {
    std::ifstream file;
    file.open(fileName);

    if (!file.is_open()) {
        printError("Could not open \"" + fileName + "\"", 2);
    }

    if (file.eof()) {
        printError("File \"" + fileName + "\" is empty", 2);
    }

    //Variables used throughout the checks
    int lineCount = 1;//Errors are better when you get what line is causing them
    int i = 0;
    std::string currentLine, buffer;
    fileValiditySequenceChecker sequenceChecker;

    //Checking for BPM
    getline(file, currentLine);//This first line has to be the BPM
    buffer = getNextWord(currentLine, i);
    if (!isAValidBPM(buffer)) {
        printError("The first line of the file has to be a number specifying the BPM. \"" + buffer + "\" is not valid.", fileName, lineCount, 2);
    }
    buffer = getNextWord(currentLine, i);
    if (!buffer.empty()) {
        printError("Invalid characters (" + buffer + ") following BPM.", 2);
    }

    lineCount++;

    while (!file.eof()) {
        getline(file, currentLine);

        if (currentLine.length() > 0) {
            i = 0;

            buffer = getNextWord(currentLine, i);

            if (isANote(buffer)) {
                checkNoteLineValidity(currentLine, fileName, lineCount);
            } else if (buffer == "P") {
                checkPauseLineValidity(currentLine, fileName, lineCount);
            } else if (buffer == ";") {
                sequenceChecker.checkEndOfSequenceDefinitionValidity(currentLine, fileName, lineCount);
            } else {
                if (!buffer.empty()) {
                    sequenceChecker.checkSequenceLineValidity(currentLine, fileName, lineCount);
                }
            }
        }

        lineCount++;
    }

    if (sequenceChecker.definingSequence) {
        printError("Never finished defining sequence \"" + sequenceChecker.sequenceBeingDefined + "\"", fileName, 2);
    }
}
