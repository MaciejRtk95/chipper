#include <string>
#include "getNextWord.h"
#include "isANumber.h"
#include "printError.h"

void checkPauseLineValidity(const std::string &line, const std::string &fileName, const int lineCount) {
    int i = 0;
    getNextWord(line, i);//Skipping over the first word ("P")

    //The next "word" must be a number specifying the duration of the pause
    std::string buffer = getNextWord(line, i);
    if (!isANumber(buffer)) {
        printError("\"" + buffer + "\" is not a valid duration", fileName, lineCount, 2);
    }

    //There shouldn't be anything after the duration
    buffer = getNextWord(line, i);
    if (!buffer.empty()) {//That would mean there's more after the number
        printError("Invalid characters following duration", fileName, lineCount, 2);
    }
}
