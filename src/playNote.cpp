#include <string>
#include <thread>
#include <iostream>
#include "getNextWord.h"
#include "stepsToSeconds.h"
#include "ProgramOptions.h"
#include "printError.h"

void passProcessedNoteToSox(std::string *note, std::string *duration, int repetitions) {
    std::string command = "play -n";
    if (programOptions.verbosity < 3) {
        command += " -q";
    }
#ifdef __linux__
    command += " -t alsa";
#endif
    command += " synth " + *duration + " exp " + *note + " vol 0.25 fade l 0.005 0 0.005";
    if (!programOptions.simulationMode) {
        for (int i = 0; i < repetitions; i++) {
            system(command.c_str());
        }
    }

    delete note;
    delete duration;
}

void playNote(const std::string &line) {
    int i = 0;
    auto *note = new std::string;
    *note = getNextWord(line, i);

    auto *duration = new std::string;
    *duration = stepsToSeconds(getNextWord(line, i));

    std::string buffer = getNextWord(line, i);
    int repetitions = 1;
    bool ampersand = false;
    if (isAnInteger(buffer)) {
        repetitions = std::stoi(buffer);
        buffer = getNextWord(line, i);
        if (!buffer.empty()) {
            ampersand = true;
        }
    } else if (!buffer.empty()) {
        ampersand = true;
    }

    if (programOptions.verbosity > 0) {
        std::cout << "Playing " << *note << " for " << *duration << " seconds";
        if (ampersand) {
            std::cout << " (not waiting)";
        }
        if (repetitions > 1) {
            std::cout << ' ' << repetitions << " times";
        }
        std::cout << '.' << std::endl;
    }

    if (ampersand) {//If this is true, then i is on a space after the duration and that space is followed by a &
        std::thread parallelNote(passProcessedNoteToSox, note, duration, repetitions);
        parallelNote.detach();
    } else {
        passProcessedNoteToSox(note, duration, repetitions);
    }
}
