#ifndef CHIPPER_SEQUENCEVECTOR_H
#define CHIPPER_SEQUENCEVECTOR_H

#include "Sequence.h"

class SequenceVector {
private:
    std::vector<Sequence> sequences;

    unsigned int find(const std::string &sequenceName) {
        for (unsigned int i = 0; i < sequences.size(); i++) {
            if (sequences[i].name == sequenceName) {
                return i;
            }
        }
        return 0;//Given the fact that there's an error check before any file playing happens, we should never get here
    }

    bool exists(const std::string &sequenceNameInQuestion) {
        for (auto &sequence : sequences) {
            if (sequence.name == sequenceNameInQuestion) {
                return true;
            }
        }
        return false;
    }

public:
    void play(const std::string &sequenceName, const int repetitions, const unsigned int indentation = 1) {
        for (int i = 0; i < repetitions; i++) {
            if (programOptions.verbosity > 0) {
                for (int j = 1; j < indentation; j++) {
                    std::cout << '\t';
                }
                std::cout << '(' << i + 1 << '/' << repetitions << ") ";
            }
            play(sequenceName, indentation);
        }
    }

    void play(const std::string &sequenceName, const unsigned int indentation = 1) {
        if (programOptions.verbosity > 0) {
            std::cout << "Playing sequence `" << sequenceName << "`." << std::endl;
        }

        const Sequence *sequence = &sequences[find(sequenceName)];
        for (auto &line : sequence->lines) {
            if (programOptions.verbosity > 0) {
                for (int i = 0; i < indentation; i++) {
                    std::cout << '\t';
                }
            }

            int i = 0;
            std::string buffer = getNextWord(line, i);

            if (isANote(buffer)) {
                playNote(line);
            } else if (buffer == "P") {
                pause(line);
            } else if (!buffer.empty()) {//Else it's either a sequence or an empty line
                std::string repetitions = getNextWord(line, i);
                if (!repetitions.empty()) {
                    for (int j = 0; j < indentation; j++) {//Tab damage control
                        std::cout << '\b';
                    }
                    play(buffer, std::stoi(repetitions), indentation + 1);
                } else {
                    play(buffer, indentation + 1);
                }
            } else if (programOptions.verbosity > 0) {//Damage control for that tab in the start of the loop.
                for (int j = 0; j < indentation; j++) {
                    std::cout << '\b';
                }
            }
        }
    }

    void define(const std::string &sequenceName, std::ifstream &file) {
        unsigned long sequenceIndex;
        if (exists(sequenceName)) {
            sequenceIndex = find(sequenceName);
            sequences[sequenceIndex].clear();

            if (programOptions.verbosity > 1) {
                std::cout << "Redefining sequence `" << sequenceName << "`...";
                std::cout.flush();
            }
        } else {
            Sequence temporarySequence;
            temporarySequence.name = sequenceName;
            sequences.push_back(temporarySequence);
            sequenceIndex = sequences.size() - 1;

            if (programOptions.verbosity > 1) {
                std::cout << "Defining sequence `" << sequenceName << "`...";
                std::cout.flush();
            }
        }

        std::string currentLine, buffer;
        int i = 0;

        getline(file, currentLine);
        buffer = getNextWord(currentLine, i);
        while (buffer != ";") {
            if (!buffer.empty()) {
                sequences[sequenceIndex].addLine(currentLine);
            }

            getline(file, currentLine);
            i = 0;
            buffer = getNextWord(currentLine, i);
        }

        if (programOptions.verbosity > 1) {
            std::cout << "\b\b Done." << std::endl;
        }
    }
};

#endif //CHIPPER_SEQUENCEVECTOR_H
