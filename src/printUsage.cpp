#include <iostream>
#include "ProgramOptions.h"

void printUsage() {
    std::cout << "Chipper is a program that reads a file with notes in it (in a specified format) and outputs them through your speakers as chiptune sounds. For more information, especially on the input files, go to `https://www.gitlab.com/matsaki95/chipper`.\n\n"
              << "Usage: " << programOptions.programCall << " [options] input_file [input_file ...]\n\n"
              << "Options:\n"
              << "  -b, --bpm           Sets a specific BPM that overrides the one in the input file.\n"
              << "  -h, --help          Displays this screen.\n"
              << "  -m, --multiplier    Multiplies the BPM by this number.\n"
              << "  -s, --simulation    The program runs in a simulation kinda mode where it functions normally but never calls SoX or pauses. Useful for testing some things a lot lot faster than normally.\n"
              << "  -v, --verbosity     Increases verbosity level." << std::endl;
    exit(0);
}
