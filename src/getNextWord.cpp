#include <string>

std::string getNextWord(const std::string &source, int &iterator) {
    //Skip whitespace
    while ((source[iterator] == ' ' || source[iterator] == '\t') && iterator < source.length()) {
        iterator++;
    }

    //Read until whitespace or end
    std::string buffer;
    while (source[iterator] != ' ' && source[iterator] != '\t' && iterator < source.length()) {
        buffer += source[iterator];
        iterator++;
    }

    return buffer;
}

std::string getNextWord(const std::string &source) {
    int i = 0;
    return getNextWord(source, i);
}
